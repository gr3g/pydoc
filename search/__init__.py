import pkgutil
import difflib
import pydoc
import builtins


def suggest(keywords=[''], suggestions_size=10):
    """Returns a list of best suggestions."""

    if len(keywords) == 0:
        distributions_keys = primary_distributions()
        keyword = ''

    elif len(keywords) == 1:
        distributions_keys = primary_distributions()
        keyword = keywords[0]

    else:
        distributions_keys = sub_distributions_keys(keywords[:-1])
        keyword = keywords[-1]

    matches_sorted = sorted(distributions_keys, key=lambda x: 1-difflib.SequenceMatcher(None, x, keyword).ratio())

    if keyword in matches_sorted:
        matches_sorted.remove(keyword)

    best_matches = matches_sorted[:min(suggestions_size, len(matches_sorted))]

    return best_matches


def primary_distributions():
    importable_modules = [i[1] for i in pkgutil.iter_modules()]
    all_builtins = dir(builtins)

    return set(importable_modules + all_builtins)


def sub_distributions_keys(distpath=['']):
    module, n = None, 0
    try:
        while n < len(distpath):
            nextmodule = pydoc.safeimport('.'.join(distpath[:n+1]), forceload=0)  # forceload=1 will crash
            if nextmodule:
                module, n = nextmodule, n + 1
            else:
                break
        if nextmodule:
            return dir(module)
        else:
            return []

    except Exception as e:
        print(e)
        return []
