"""
Enables users to search trough the pydoc.
"""
from albertv0 import *
from os import path
import requests
import pydoc
import pkgutil
from textwrap import TextWrapper
from themes import Theme
import themes
from search import suggest


__iid__ = "PythonInterface/v0.2"
__prettyname__ = 'PydocSearch'
__version__ = '1.0'
__trigger__ = 'doc '
__author__ = 'Grégoire Devauchelle'
__dependencies__ = []
__icon__ = path.dirname(__file__) + '/icons/python.svg'


REQUEST_HEADERS = {
    'User-Agent': (
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
        ' Chrome/62.0.3202.62 Safari/537.36'
    )
}
session = requests.Session()
session.trust_env = False

packages_list = [ele[1] for ele in pkgutil.iter_modules()]
tw = TextWrapper(width=80)

theme = Theme(theme="Default")


def to_item(text, id):
    return Item(
        id=id,
        text=text,
        icon=__icon__,
        actions=[
            ClipAction("Copy result to clipboard", text)
        ]
    )


def search(query):

    qlevels = query_levels(query)
    suggestions = suggest(qlevels)

    try:
        pydoc_arr = pydoc.plain(pydoc.render_doc(query, title='%s')).split('\n')
        lines = themes.preprocess(pydoc_arr)
        lines = [theme.apply(line, query) for line in lines]

        text = '<br>'.join(lines)

    except Exception as e:
        text = theme.apply("Could not find Python documentation for {0}".format(query), query)

    """ Add a suggestion line """
    if len(suggestions) > 0:
        text += "<br><br>" + theme.apply("Try also: <i>" + ", ".join(suggestions) + "</i>", query=query)

    id = '1'

    return [to_item(text, id)]


def handleQuery(query):
    if query.isTriggered and len(query.string) > 0:
        return search(query.string)
    # return [Item(icon=__icon__, text='Start typing `doc <i>module</i><br> and a second title', subtext='This is a subtext <br> and a second one')]


def query_levels(query):
    return query.split('.')
