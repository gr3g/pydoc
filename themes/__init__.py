from .themes import Theme


def preprocess(lines):
    # Remove empty lines
    #lines = [line for line in lines if len(line.strip(" ")) > 0]

    # Put on the same line the sections' title and sections' content
    sections = []
    continue_section = False
    for line in lines[:10]: # we limit to 10 lines as we do not know how to scroll yet
        if continue_section:
            sections[-1] = sections[-1] + line
            continue_section = False
        else:
            if line.lower() in ["name", "description"]:
                sections.append("<i>{}: </i>".format(line.title()))
                continue_section = True
            else:
                sections.append(line)
                continue_section = False

    return sections
