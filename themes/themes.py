import os

try:
    from bs4 import BeautifulSoup
    xml_compatible = True
except Exception as e:
    print("Could not import BeautifulSoup from bs4:", e)
    xml_compatible = False

import json


class Theme:
    _folder_xml = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'xml')
    _folder_json = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'json')

    def __init__(self, theme="Default", load_from="json"):
        self.theme = theme

        if load_from == "json":
            self.json_parser = self._load_json()
            self.font = {
                "keyword": {"color": self.json_parser["KEYWORD"]["FONT_COLOR"]},
                "class": {"color": self.json_parser["CLASS_DECLARATION"]["FONT_COLOR"]},
                "function": {"color": self.json_parser["FUNCTION_DECLARATION"]["FONT_COLOR"]},
                "package": {"color": self.json_parser["PACKAGE_DECLARATION"]["FONT_COLOR"]},
                "default": {"color": self.json_parser["DEFAULT"]["FONT_COLOR"]}

            }
        elif load_from == "xml" and xml_compatible:
            self.xml_parser = self._load_xml()
            self.font = {
                "keyword": {"color": self.xml_parser.scheme.attributes.find("option", {"name": "KEYWORD"}).value.option["value"]},
                "class": {"color": self.xml_parser.scheme.attributes.find("option", {"name": "CLASS_DECLARATION"}).value.option["value"]},
                "function": {"color": self.xml_parser.scheme.attributes.find("option", {"name": "FUNCTION_DECLARATION"}).value.option["value"]},
                "package": {"color": self.xml_parser.scheme.attributes.find("option", {"name": "PACKAGE_DECLARATION"}).value.option["value"]},
                "default": {"color": self.xml_parser.scheme.attributes.find("option", {"name": "DEFAULT"}).value.option["value"]}

            }
        else:
            print("Value for parameter load_from: {} is not valid.".format(load_from))

    def apply(self, mystring, query):
        package_keyword = "package"
        class_keyword = "class"

        if query and '.' in query:
            if query[-1:] == '.':
                subquery = query[:-1]
            else:
                subquery = query[query.rfind('.') + 1:]
        else:
            subquery = query

        starts_wkeyword = False
        sublines = []
        for keyword in [package_keyword, class_keyword]:
            if mystring.startswith(keyword):
                sublines.append("<i>" + self.apply_font(keyword, keyword="keyword") + "</i>")
                sublines.append("<b>" + self.apply_font(subquery, keyword="package") + "</b>")
                sublines.append(self.apply_font(mystring[len(keyword) + 1 + len(subquery):]))
                starts_wkeyword = True
                break

        if not starts_wkeyword:
            sublines.append(self.apply_font(mystring))

        return " ".join(sublines)

    def apply_font(self, mystring, keyword='default', size='2'):
        return "<font color='#{0}' size='{1}'>".format(self.font[keyword]["color"], size) + mystring + "</font>"

    def _load_xml(self):
        with open(os.path.join(Theme._folder_xml, '{}.xml'.format(self.theme)), 'r') as theme_file:
            theme_xml = theme_file.read()

        return BeautifulSoup(theme_xml, features="xml")

    def _load_json(self):
        with open(os.path.join(Theme._folder_json, '{}.json'.format(self.theme)), 'r') as theme_file:
            theme_json = json.loads(theme_file.read())
        return theme_json