# Albert plugin for pydoc search

## Requirements
This plugins leverages the QML Box Model of Albert launcher. Therefore it is recommended to have:
* Alber launcher 0.13 or more
* QML Box Model as frontend (Albert launcher > Settings > General > frontend)

![](images/albert launcher settings.png)

## Installation
To install this python plugin:
1. copy this repository into "<PATH TO ALBERT>/albert/org.albert.extension.python/modules".
2. open albert launcher settings
3. Tick the boxes "Extensions > Python > PydocSearch"

You should be able to start using the pluging now (see section "Description").

## Description
The trigger word for this plugin is "doc". For examples, see the section "Examples" below.

## Examples
### Suggestions for query 'doc st'
![](images/albert launcher pydoc example st.png)

### Result for query 'doc str'
![](images/albert launcher pydoc example str.png)

### Result for query 'doc pandas'
![](images/albert launcher pydoc example pandas.png)

### Suggestions for query 'doc pandas.Se'
![](images/albert launcher pydoc example pandas.Se.png)

### Result for query 'doc pandas.Series'
![](images/albert launcher pydoc example pandas.Series.png)

## Contributions
This project is at its very beginning. Please feel free to:
* Suggest any improvement
* Create PRs
* Redirect me to any tool that already does the job. No need to reinvent the wheel.

Thanks and I hope you will find this useful.